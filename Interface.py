from tkinter import *
from tkinter import filedialog

from Routeur import Routeur
from Lien import Lien
from Graphe import creation_graphe
from AlgoRoutage import *


class Interface(Frame):

    def __init__(self, liste_routeurs, liste_liens):
        Frame.__init__(self, width=1100, height=900)
        self.canvas = Canvas(self, width=1100, height=600)
        self.canvas.bind("<Button-1>", self.callback)
        self.canvas.pack()

        new_graphe = Button(text="Générer nouveau graphe", command=self.nouveau_graphe)
        new_graphe.pack()

        start = Button(text="Start", command=self.start)
        start.pack()

        import_btn = Button(text="Importer", command=self.importer)
        import_btn.pack()

        export_btn = Button(text="Exporter", command=self.exporter)
        export_btn.pack()

        self.liste_routeurs = liste_routeurs
        self.liste_liens = liste_liens
        self.depart = False
        self.routeur_depart = None
        self.arrivee = False
        self.routeur_arrivee = None
        self.error = False
        self.folder_path = StringVar()
        self.liste_liens_select = []
        self.liste_clpc = []

        self.print()

        self.pack(fill=BOTH)
        self.nb_clic = 0

        self.mainloop()

    def print(self):

        self.canvas.delete("all")

        for lien in self.liste_liens:
            self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                    lien.routeurs[1].y + 50)
            min_x = min(lien.routeurs[0].x + 50, lien.routeurs[1].x + 50)
            min_y = min(lien.routeurs[0].y + 50, lien.routeurs[1].y + 50)
            x_middle = min_x + abs(lien.routeurs[0].x - lien.routeurs[1].x) / 2
            y_middle = min_y + abs(lien.routeurs[0].y - lien.routeurs[1].y) / 2
            self.canvas.create_text(x_middle, y_middle, fill="darkblue", font="Times 10 italic bold",
                                    text=lien.cout)

        if not len(self.liste_liens_select) == 0:
            for lien in self.liste_liens_select:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill="orange", width=4)
                min_x = min(lien.routeurs[0].x + 50, lien.routeurs[1].x + 50)
                min_y = min(lien.routeurs[0].y + 50, lien.routeurs[1].y + 50)
                x_middle = min_x + abs(lien.routeurs[0].x - lien.routeurs[1].x) / 2
                y_middle = min_y + abs(lien.routeurs[0].y - lien.routeurs[1].y) / 2
                self.canvas.create_text(x_middle, y_middle, fill="darkblue", font="Times 10 italic bold",
                                        text=lien.cout)

        if not len(self.liste_clpc) == 0:
            for lien in self.liste_clpc:
                self.canvas.create_line(lien.routeurs[0].x + 50, lien.routeurs[0].y + 50, lien.routeurs[1].x + 50,
                                        lien.routeurs[1].y + 50, fill="red", width=4)
                min_x = min(lien.routeurs[0].x + 50, lien.routeurs[1].x + 50)
                min_y = min(lien.routeurs[0].y + 50, lien.routeurs[1].y + 50)
                x_middle = min_x + abs(lien.routeurs[0].x - lien.routeurs[1].x) / 2
                y_middle = min_y + abs(lien.routeurs[0].y - lien.routeurs[1].y) / 2
                self.canvas.create_text(x_middle, y_middle, fill="darkblue", font="Times 10 italic bold",
                                        text=lien.cout)

        for routeur in self.liste_routeurs:
            self.canvas.create_oval(routeur.x - 10 + 50, routeur.y - 10 + 50, routeur.x + 10 + 50, routeur.y + 10 + 50,
                                    fill="gray")
            self.canvas.create_text(routeur.x + 50, routeur.y + 50, fill="white", font="Times 10 italic bold",
                                    text=routeur.id)

        if self.arrivee:
            self.canvas.create_oval(self.routeur_arrivee.x - 10 + 50, self.routeur_arrivee.y - 10 + 50,
                                    self.routeur_arrivee.x + 10 + 50, self.routeur_arrivee.y + 10 + 50,
                                    fill="red")
            self.canvas.create_text(self.routeur_arrivee.x + 50, self.routeur_arrivee.y + 50, fill="white",
                                    font="Times 10 italic bold",
                                    text=self.routeur_arrivee.id)

        if self.depart:
            self.canvas.create_oval(self.routeur_depart.x - 10 + 50, self.routeur_depart.y - 10 + 50,
                                    self.routeur_depart.x + 10 + 50, self.routeur_depart.y + 10 + 50,
                                    fill="green")
            self.canvas.create_text(self.routeur_depart.x + 50, self.routeur_depart.y + 50, fill="white",
                                    font="Times 10 italic bold",
                                    text=self.routeur_depart.id)

    def exporter(self):
        filename = filedialog.asksaveasfilename(initialdir="/", title="Select file",
                                                 filetypes=(("text files", "*.txt"), ("all files", "*.*")))
        self.folder_path.set(filename)
        text = ""
        for routeur in self.liste_routeurs:
            text += str(routeur.id) +","+ str(routeur.x) +","+ str(routeur.y)+";"
        text += "/"
        for lien in self.liste_liens:
            text += str(lien.id)+","+str(lien.routeurs[0].id)+","+str(lien.routeurs[1].id)+","+str(lien.cout)+","+str(lien.delai)+";"
        with open(filename+".txt","a") as file:
            file.write(text)
        return 0

    def importer(self):
        filename = filedialog.askopenfilename(initialdir="/", title="Selectionner fichier",
                                              filetypes=(("text files", "*.txt"), ("all files", "*.*")))
        self.folder_path.set(filename)
        with open(filename) as file:
            text = file.read()
            routeurs, liens = text.split("/")
            liste_routeurs = routeurs.split(";")
            liste_liens = liens.split(";")
            liste_new_routeurs = []
            liste_new_liens = []
            for routeur in liste_routeurs:
                if not routeur == '':
                    print(routeur)
                    info_routeur = routeur.split(",")
                    new_routeur = Routeur(int(info_routeur[0]), int(info_routeur[1]), int(info_routeur[2]))
                    liste_new_routeurs.append(new_routeur)
            for lien in liste_liens:
                if not lien == '':
                    info_lien = lien.split(",")
                    liste_routeurs_lien = []
                    for routeur in liste_new_routeurs:
                        if routeur.id == int(info_lien[1]) or routeur.id == int(info_lien[2]):
                            liste_routeurs_lien.append(routeur)
                    new_lien = Lien(int(info_lien[0]), liste_routeurs_lien[0], liste_routeurs_lien[1], int(info_lien[3]),
                                    int(info_lien[4]))
                    liste_new_liens.append(new_lien)
                    for routeur in liste_new_routeurs:
                        if routeur == new_lien.routeurs[0] or routeur == new_lien.routeurs[1]:
                            routeur.liens.append(new_lien)
        self.liste_liens = liste_new_liens
        self.liste_routeurs = liste_new_routeurs
        self.print()

    def start(self):
        self.liste_clpc = []
        self.liste_liens_select = []
        if self.depart and not self.arrivee:
            self.liste_liens_select = arbre_min(30, self.routeur_depart)
            self.print()
        else:
            [self.liste_liens_select, self.liste_clpc] = plus_court_chemin(self.routeur_depart, self.routeur_arrivee)
            self.print()

    def nouveau_graphe(self):
        self.reset()
        self.liste_routeurs, self.liste_liens = creation_graphe()
        self.print()

    def callback(self, event):
        x = event.x
        y = event.y
        for routeur in self.liste_routeurs:
            if routeur.x + 50 - 10 < x < routeur.x + 50 + 10 and routeur.y + 50 - 10 < y < routeur.y + 50 + 10:
                if not self.depart:
                    if not routeur == self.routeur_arrivee:
                        self.routeur_depart = routeur
                        self.depart = True
                    else:
                        self.arrivee = False
                        self.routeur_arrivee = None
                else:
                    if self.routeur_depart == routeur:
                        self.depart = False
                        self.routeur_depart = None
                    else:
                        if not self.arrivee:
                            self.arrivee = True
                            self.routeur_arrivee = routeur
                        else:
                            if self.routeur_arrivee == routeur:
                                self.arrivee = False
                                self.routeur_arrivee = None
                            else:
                                self.routeur_arrivee = routeur
        self.print()

    def reset(self):
        self.liste_routeurs = []
        self.liste_liens = []
        self.depart = False
        self.routeur_depart = None
        self.arrivee = False
        self.routeur_arrivee = None
        self.liste_liens_select = []
        self.liste_clpc = []
