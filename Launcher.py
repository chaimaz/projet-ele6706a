from Interface import Interface
from Graphe import creation_graphe

if __name__ == "__main__":
    liste_routeurs, liste_lien = creation_graphe()
    interface = Interface(liste_routeurs, liste_lien)
