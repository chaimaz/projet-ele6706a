class Routeur:

    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y
        self.liens = []

    def __str__(self):
        return str("Routeur {}".format(self.id))

