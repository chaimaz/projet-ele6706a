
def arbre_min(nb_routeur, routeur_depart):
    liste_routeurs_select = [routeur_depart]
    liste_liens_select = []
    nb_routeur_couverts = 1
    while not nb_routeur_couverts == nb_routeur:
        liste_lien = []
        for routeur in liste_routeurs_select:
            liste_lien = liste_lien + routeur.liens
        lien_select = None
        for lien in liste_lien:
            if not (lien.routeurs[0] in liste_routeurs_select and lien.routeurs[1] in liste_routeurs_select):
                if lien_select is not None:
                    if lien_select.cout > lien.cout:
                        lien_select = lien
                else:
                    lien_select = lien
        liste_liens_select.append(lien_select)
        if lien_select.routeurs[0] in liste_routeurs_select:
            liste_routeurs_select.append(lien_select.routeurs[1])
        else:
            liste_routeurs_select.append(lien_select.routeurs[0])
        nb_routeur_couverts += 1
    return liste_liens_select


def plus_court_chemin(routeur_depart, routeur_arrivee):
    liste_routeurs_select = [[routeur_depart, 0]]
    liste_routeurs_simple = [routeur_depart]
    liste_liens_select = []
    routeur_fin = None
    while not routeur_fin == routeur_arrivee:
        liste_liens = []
        for routeur in liste_routeurs_select:
            liste_liens += routeur[0].liens
        min_temp = -1
        save_routeur = None
        lien_select = None
        for lien in liste_liens:
            if not (lien.routeurs[0] in liste_routeurs_simple and lien.routeurs[1] in liste_routeurs_simple):
                for routeur in liste_routeurs_select:
                    if routeur[0] == lien.routeurs[0] or routeur[0] == lien.routeurs[1]:
                        cout = routeur[1] + lien.cout
                        if min_temp < 0 or cout < min_temp:
                            lien_select = lien
                            min_temp = cout
                            if routeur[0] == lien.routeurs[0]:
                                save_routeur = lien.routeurs[1]
                            else:
                                save_routeur = lien.routeurs[0]
        liste_routeurs_simple.append(save_routeur)
        liste_routeurs_select.append([save_routeur, min_temp])
        liste_liens_select.append(lien_select)
        routeur_fin = save_routeur
    liste_clpc = []
    # routeur_actuel = routeur_arrivee
    # liste_liens_copie = liste_liens_select
    # lien_save = None
    # while not routeur_actuel == routeur_depart:
    #     cout = -1
    #     for lien in liste_liens_copie:
    #         if lien.routeurs[0] == routeur_actuel or lien.routeurs[1] == routeur_actuel:
    #             if lien.routeurs[0] == routeur_actuel:
    #                 routeur = lien.routeurs[1]
    #             else:
    #                 routeur= lien.routeurs[0]
    #             for routeur_select in liste_routeurs_select:
    #                 if routeur_select[0] == routeur:
    #                     if cout < 0 or routeur_select[1] < cout:
    #                         lien_save = lien
    #             liste_liens_copie.remove(lien)
    #     print(lien_save)
    #
    #     liste_clpc.append(lien_save)
    #     if lien_save.routeurs[0] == routeur_actuel:
    #         routeur_actuel = lien_save.routeurs[1]
    #     else:
    #         routeur_actuel = lien_save.routeurs[0]
    #     print(routeur_actuel)
    liste = [liste_liens_select, liste_clpc]
    return liste